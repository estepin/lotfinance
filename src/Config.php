<?php
/**
 * Created by PhpStorm.
 * Project: g-mfo
 * User: Anton Neverov <neverov12@gmail.com>
 * Date: 25.07.2018
 * Time: 17:46
 */

namespace MfoRu\MfoAccounting\z400;

class Config
{
    public $token;
    public $customer_key;

    public $crmToken;
    public $crmUrl;
}