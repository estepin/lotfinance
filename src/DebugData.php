<?php
/**
 * Created by PhpStorm.
 * Project: g-mfo
 * User: Anton Neverov <neverov12@gmail.com>
 * Date: 27.07.2018
 * Time: 13:36
 */

namespace MfoRu\MfoAccounting\z400;


class DebugData implements \MfoRu\Contracts\MfoAccounting\DebugData
{

    private $data = null;

    public function __construct($result)
    {
        $this->data = $result;
    }

    function getRequestHeader():string
    {
        return http_build_query($this->data['request_headers']);
    }

    function getRequestBody():string
    {
        return $this->data['request_body'];
    }

    function getResponseHeader():string
    {
        return http_build_query($this->data['response_headers']);
    }

    function getResponseBody():string
    {
        return $this->data['response_body'];
    }
}