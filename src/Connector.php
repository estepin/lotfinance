<?php
/**
 * Created by PhpStorm.
 * Project: g-mfo
 * User: Anton Neverov <neverov12@gmail.com>
 * Date: 25.07.2018
 * Time: 17:47
 */

namespace MfoRu\MfoAccounting\z400;

use MfoRu\Contracts\MfoAccounting\Anket;
use MfoRu\Contracts\MfoAccounting\DebugData as InterfaceDebugData;
use MfoRu\Contracts\MfoAccounting\Exceptions\DuplicateClient;
use MfoRu\Contracts\MfoAccounting\Exceptions\NotAuth;
use MfoRu\MfoAccounting\z400\Exceptions\WrongToken;
use MfoRu\MfoAccounting\z400\Exceptions\DoubleUser;
use MfoRu\MfoAccounting\z400\Config;
use MfoRu\MfoAccounting\z400\DebugData;

class Connector implements \MfoRu\Contracts\MfoAccounting\Connector
{

    protected $debugData = null;

    function addToUpload(Anket $anket, $config)
    {
        return $this->sendRequest($anket, $config);
    }

    protected function sendRequest(Anket $anket, $config)
    {
        $formatter = new Formatter();
        $apiClient = new Api();
        $apiCrm = new ApiCrm($config->crmUrl, $config->crmToken);

        $arrAnket = $formatter->anketToApiArray($anket);
        $result = $apiClient->sendRequest($arrAnket, $config);

        $this->debugData = $result['debug'];

        if(isset($result['result']->success) && $result['result']->success === true)
        {
            $id = $result['result']->id;
            if(!$resData = $apiCrm->addAnketId($id) || !isset($resData->success) || !$resData->success)
            {
                throw new \Exception('Lotcrm unknown response');
            }

            return $result['result']->id;
        }
        elseif($result['result']->error_code == 403)
        {
            throw new NotAuth($result['result']->errors[0]);
        }
        elseif($result['result']->error_code == 422)
        {
            $error = $result['result']->errors->input->title.': ';
            foreach ($result['result']->errors->input->errors as $e) {
                $error .= ', '.$e[0].' - '.$e[1][0];
            }
            throw new DuplicateClient($error);
        }
    }

    function getAccData($id)
    {
        // TODO: Implement getAccData() method.
    }

    function getConfigModel()
    {
        return new Config();
    }

    function checkStatus($anketId, $config)
    {
        $apiCrm = new ApiCrm($config->crmUrl, $config->crmToken);
        $data = $apiCrm->getStatus($anketId);

        if(isset($data->status) && $data->status == 'issued')
        {
            return self::STATUS_ISSUED;
        }
        return self::STATUS_SENDED;
    }

    function getDebugData():InterfaceDebugData
    {
        if ($this->debugData !== null) {
            return new DebugData($this->debugData);
        } else{
            return false;
        }
    }

    function testConfig($config): bool
    {
        $anket = $this->getTestAnket();
        try{
            $result = $this->sendRequest($anket, $config);
        } catch (WrongToken $e) {
            return false;
        } catch (DoubleUser $e) {
            return true;
        }
        return (bool) $result;
    }

    function getTestAnket()
    {
        $anket = new Anket();

        $anket->summ = '1000';
        $anket->term = '10';

        $anket->lastname = 'Петров';
        $anket->firstname = 'Петр';
        $anket->middlename = 'Петрович';
        $anket->gender = '1';
        $anket->birthdate = '1990-01-01';

        $anket->email = 'test@mfo.ru';
        $anket->phone = '+79091112233';

        $anket->passSeriaNum = '11 26 123321';
        $anket->passWhoIssued = 'УВД';
        $anket->passCode = '123-321';
        $anket->passDate = '2011-01-01';
        $anket->passBirthPlace = 'Саратов';

        $anket->regIndex = '410001';
        $anket->regRegion = 'Саратовская обл';
        $anket->regCity = 'Саратов';
        $anket->regStreet = 'Московская';
        $anket->regHouse = '1';
        $anket->regRoom = 1;

        $anket->liveIndex = '410001';
        $anket->liveRegion = 'Саратовская обл';
        $anket->liveCity = 'Саратов';
        $anket->liveStreet = 'Московская';
        $anket->liveHouse = '1';
        $anket->liveRoom = 1;

        return $anket;
    }

}