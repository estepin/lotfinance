<?php
/**
 * Created by PhpStorm.
 * Project: g-mfo
 * User: Anton Neverov <neverov12@gmail.com>
 * Date: 25.07.2018
 * Time: 17:47
 */

namespace MfoRu\MfoAccounting\z400;

use MfoRu\Contracts\MfoAccounting\Anket;

class Formatter
{
    function anketToApiArray(Anket $anket)
    {

        $arr = [
            'last_name' => $anket->lastname,
            'first_name' => $anket->firstname,
            'middle_name' => $anket->middlename,
            'mobile_phone' => $this->formatPhone($anket),
            'email' => $anket->email,
            'accept_condition' => 1
        ];

        return $arr;
    }

    function formatPhone(Anket $anket)
    {
        if(preg_match( '/^(\+)??(\d)(\d{3})(\d{7})$/', $anket->phone,  $matches))
        {
            $result = str_replace('+', '', $anket->phone);
            return $result;
        }
        else
        {
            throw new \Exception('Wrong phone');
        }
    }
}