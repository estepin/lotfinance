<?php
/**
 * Created by PhpStorm.
 * Project: g-mfo
 * User: Anton Neverov <neverov12@gmail.com>
 * Date: 25.07.2018
 * Time: 17:44
 */

namespace MfoRu\MfoAccounting\z400;

use GuzzleHttp\Client;

class Api
{
    protected $client;
    protected $formatter;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function sendRequest($arRequest, $config)
    {

        $client = new Client();
        $res = $client->request(
            'POST',
            'https://z400.ru/api/user/create/',
            [
                'headers' => [
                    'customer_key' => $config->customer_key,
                    'token' => $config->token
                ],
                'verify' => false,
                'exceptions' => false,
                'body' => json_encode($arRequest)
            ]
        );

        $body = $res->getBody()->getContents();

        $return = [
            'debug' => [
                'request_headers' => [
                    'customer_key' => $config->customer_key,
                    'token' => $config->token
                ],
                'request_body' => json_encode($arRequest),
                'response_headers' => $res->getHeaders(),
                'response_body' => $body
            ]
        ];

        foreach ($client->getConfig()['headers'] as $key => $header){
            $return['debug']['request_headers'][$key] = $header;
        }

        $return['result'] = \GuzzleHttp\json_decode($body);

        return $return;
    }
}