<?php
namespace MfoRu\MfoAccounting\z400;

use GuzzleHttp\Client;

class ApiCrm
{
    protected $server;
    protected $token;

    function __construct($url, $token)
    {
        $this->server = $url;
        $this->token = $token;

        $this->client = new Client();
    }

    function getStatus($anketId)
    {
        $content = $this->client->get($this->server.'?token='.$this->token.'&anketId='.$anketId.'&action=get-status');
        $content = $content->getBody()->getContents();
        if($content)
        {
            if($data = json_decode($content))
                return $data;
            else
                throw new \Exception('Unknown response lotcrm');
        }
        else
        {
            throw new \Exception('Unknown response lotcrm');
        }
    }

    function addAnketId($anketId)
    {
        $content = $this->client->get($this->server.'?token='.$this->token.'&anketId='.$anketId.'&action=add-anket');
        $content = $content->getBody()->getContents();
        if($content)
        {
            if($data = json_decode($content))
                return $data;
            else
                throw new \Exception('Unknown response lotcrm');
        }
        else
        {
            throw new \Exception('Unknown response lotcrm');
        }
    }

}